#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_TB300XU.mk

COMMON_LUNCH_CHOICES := \
    omni_TB300XU-user \
    omni_TB300XU-userdebug \
    omni_TB300XU-eng
