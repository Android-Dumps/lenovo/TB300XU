#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from TB300XU device
$(call inherit-product, device/lenovo/TB300XU/device.mk)

PRODUCT_DEVICE := TB300XU
PRODUCT_NAME := lineage_TB300XU
PRODUCT_BRAND := Lenovo
PRODUCT_MODEL := TB300XU
PRODUCT_MANUFACTURER := lenovo

PRODUCT_GMS_CLIENTID_BASE := android-lenovo

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="sys_mssi_t_32_ago_ww-user 12 SP1A.210812.016 S001088_230331_ROW release-keys"

BUILD_FINGERPRINT := Lenovo/TB300XU_S/TB300XU:12/SP1A.210812.016/S001088_230331_ROW:user/release-keys
