#!/system/bin/sh

if [ -z "$1" ]; then        
	SAVEFLAG=$(getprop persist.sys.lenovo.log.save)
else
        SAVEFLAG=$1
fi


LOGDISK=$(getprop persist.sys.lenovo.log.disk)
LOGFOLDER=$(getprop persist.sys.lenovo.log.folder)

LASTKMSG="/data/local/log/lastkmsg" 
LASTLOG="/data/local/log/lastlog"
WLAN_INI="/system/etc/wifi/WCNSS_qcom_cfg.ini" 
GPSLOG_DIR=/data/gps/log
ANR_DIR=/data/anr
RECOVERY_DIR=/cache/recovery
CRASH_DIR=/data/tombstones
#BT_ENABLE=$APLOG_DIR/bluetooth.enable
BT_DIR=/data/misc/bluedroid
BT_LOG=/data/misc/bluetooth/logs
BT_ETC_DIR=/system/etc/bluetooth
WLAN_DIR=/data/misc/wifi
DRPOBOX_DIR=/data/system/dropbox
RESCUE_DIR=/data/system/rescue_info
WLAN_LOG=/sdcard/wlan_logs
MMI_LOG=/sdcard/faclog
MAIN_LOG=/data/local/log/log
if [ ! -e $LOGDISK"/log" ]; then
	mkdir -p $LOGDISK"/log"
fi

# 
if [ $SAVEFLAG = TRUE ]; then

	APLOG_DIR=$LOGDISK"/log/"$LOGFOLDER

	cp -rf $MAIN_LOG/* $LOGDISK"/log"
	rm -rf $MAIN_LOG/*

	if [ ! -e $APLOG_DIR ]; then 
		mkdir -p $APLOG_DIR
	fi

	ps -e > $APLOG_DIR/ps.txt
	top -n 1 > $APLOG_DIR/top.txt
	cat /proc/interrupts > $APLOG_DIR/interrupts.txt
	cat /proc/cmdline > $APLOG_DIR/cmdline.txt
	cat /proc/meminfo > $APLOG_DIR/meminfo.txt
#	cat /sys/hwinfo/* > $APLOG_DIR/hwinfo.txt
#	cat /sys/devices/platform/soc/1d84000.ufshc/host0/target0:0:0/0:0:0:0/vendor >> $APLOG_DIR/hwinfo.txt
#	cat /sys/devices/platform/soc/1d84000.ufshc/host0/target0:0:0/0:0:0:0/model >> $APLOG_DIR/hwinfo.txt
#	cat /sys/devices/platform/soc/1d84000.ufshc/host0/target0:0:0/0:0:0:0/rev >> $APLOG_DIR/hwinfo.txt
	iptables -L > $APLOG_DIR/iptables.txt
	getprop > $APLOG_DIR/prop.txt
	[ -e /system/build.prop ] && cp /system/build.prop $APLOG_DIR/
	[ -e /system/etc/version.conf ] && cp /system/etc/version.conf $APLOG_DIR/
        
	DATAAPLOG="/data/local/log/aplog"
	[ -d $DATAAPLOG ] && dumpsys alarm > $DATAAPLOG/alarm.txt
	[ -d $DATAAPLOG ] && dumpsys power > $DATAAPLOG/power.txt
	[ -d $DATAAPLOG ] && dumpsys cpuinfo > $DATAAPLOG/cpuinfo.txt
        [ -d $DATAAPLOG ] && dumpsys meminfo > $DATAAPLOG/dumpmeminfo.txt
        [ -d $DATAAPLOG ] && dumpsys usagestats > $DATAAPLOG/usagestats.txt
	[ -d $DATAAPLOG ] && cp -a $DATAAPLOG/* $APLOG_DIR && cd $DATAAPLOG && rm -rf *

	[ -d $BT_ETC_DIR ] && cp -a $BT_ETC_DIR $APLOG_DIR/bluetooth
	[ -d $BT_DIR ] && cp -rf $BT_DIR/* $APLOG_DIR/bluetooth
	[ -d $BT_LOG ] && cp -rf $BT_LOG/* $APLOG_DIR/bluetooth
	[ -d $WLAN_DIR ] && cp -a $WLAN_DIR $APLOG_DIR/wlan
	[ -e $WLAN_INI ] && cp $WLAN_INI $APLOG_DIR/wlan

	cd $APLOG_DIR && chown -R media_rw:media_rw alarm.txt power.txt cpuinfo.txt dumpmeminfo.txt bluetooth \
						wlan ps.txt top.txt interrupts.txt meminfo.txt prop.txt build.prop version.conf

	cd $APLOG_DIR  && rm -fr gps anr recovery tombstones dropbox
	[ -d $LASTKMSG ] && cp -a $LASTKMSG $APLOG_DIR/lastkmsg && cd $LASTKMSG && rm -rf *
	[ -d $LASTLOG ] && cp -a $LASTLOG $APLOG_DIR/lastlog && cd $LASTLOG && rm -rf *
	[ -d $GPSLOG_DIR ] && cp -a $GPSLOG_DIR $APLOG_DIR/gps && cd $GPSLOG_DIR
	[ -d $ANR_DIR ] &&  cp -a $ANR_DIR $APLOG_DIR/anr
	[ -d $RECOVERY_DIR ] && cp -a $RECOVERY_DIR $APLOG_DIR/recovery
	[ -d $CRASH_DIR ] && cp -a $CRASH_DIR $APLOG_DIR/tombstones
	[ -d $DRPOBOX_DIR ] && cp -a $DRPOBOX_DIR $APLOG_DIR/dropbox
	[ -d $RESCUE_DIR ] && cp -a $RESCUE_DIR $APLOG_DIR/rescue
	[ -d $WLAN_LOG ] && cp -a $WLAN_LOG $APLOG_DIR/wlan_logs
	[ -d $MMI_LOG ] && cp -a $MMI_LOG $APLOG_DIR/faclog

	cd $APLOG_DIR && chown -R media_rw:media_rw lastkmsg lastlog gps anr recovery tombstones dropbox rescue

fi

if [ $SAVEFLAG = CLEAN ]; then
	cd $LOGDISK

	if [ -e $LOGDISK"/log" ]; then
    	        cd log
		if [ $(getprop persist.sys.lenovo.log) = TRUE ]; then
		    rm -rf !($LOGFOLDER)
		else
		    rm -rf *
		fi
	fi
fi

if [ $SAVEFLAG = FALSE ]; then
	cd $LOGDISK

	if [ -e $LOGDISK"/log" ]; then
    	        cd log
		if [ $(getprop persist.sys.lenovo.log) = TRUE ]; then
		    rm -rf !($LOGFOLDER)
		else
		    #rm -rf *
                    cd $LOGDISK
                    rm -rf log
		fi
	fi

	cd $LOGDISK
	if [ -e $LOGDISK"/log_out" ]; then
    	        #cd log_out
		#rm -rf *
                rm -rf log_out		
	fi
fi

/system/bin/cmd activity broadcast -a android.lenovo.action.SAVE_LENOVO_LOG_DONE -p com.lenovo.loggerpannel --es path $LOGDISK"/log"  


